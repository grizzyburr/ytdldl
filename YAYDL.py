#!/usr/bin/python3
import subprocess, os, urllib.request, re, time
import PySimpleGUI as sg

ytdlp_exe = os.path.join(os.environ['LOCALAPPDATA'], 'cavecomputing', 'ytdlp', 'yt-dl.exe')
if not os.path.exists(os.path.dirname(ytdlp_exe)):
    os.makedirs(os.path.dirname(ytdlp_exe))
if not os.path.exists(ytdlp_exe):
    urllib.request.urlretrieve('https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp.exe', ytdlp_exe)
else:
    layout_checkUpdate = [[sg.Text('Checking for yt-dlp update')]]
    window_updateCheck = sg.Window('Update', layout_checkUpdate)
    event, values = window_updateCheck.read(timeout=100)
    ytdl_update = subprocess.run('{} -U'.format(
        ytdlp_exe))  # This needs to be a Popen so I can query the output and dynamically update the pysimplegui output, need to figure out how to capture input and stuff though
    window_updateCheck.Close()
    if ytdl_update.returncode != 0:
        sg.Popup('Updating yt-dlp failed')
        exit()


def main():
    url_address, local_path = prompt()
    ytdlp_download(url_address, local_path)


def prompt():
    layout = [[sg.Text('URL: '), sg.InputText()],
              [sg.Text('Save to: '), sg.InputText(), sg.FolderBrowse()],
              [sg.OK(), sg.Cancel()]]
    window = sg.Window('Enter URL', layout)

    while True:
            event, value = window.read()
            if event == 'OK':
                if re.match(r'https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)', value[0]):
                    window.Close()
                    downloads_path = os.path.join(value[1],r'%(title)s.%(ext)s')
                    return_value = (value[0],downloads_path)
                    return return_value
                    break
                else:
                    sg.Popup('That is an invalid URL')
            elif event == 'Cancel':
                window.Close()
                exit()
            else:
                window.Close()
                exit()


def ytdlp_download(source_url, local_destination):
    layout_download_in_progress = [[sg.Text('Download in progress...')]]
    layout_done = [[sg.Text('File downloaded to {}'.format(local_destination))],
                   [sg.OK(), sg.Button('Browse')]]
    window_done = sg.Window('Complete', layout_done)
    window_progress = sg.Window('Progress', layout_download_in_progress)
    try:
        window_progress.read(timeout=100)
        result = subprocess.Popen('{} -f best -o {} {}'.format(ytdlp_exe,
                                                    local_destination,
                                                    source_url),stdout=subprocess.PIPE)
        window_progress.Read(timeout=100)
        retval = result.wait()
        window_progress.Close()

        if retval == 0:
            event, value = window_done.read()
            if event == 'Browse':
                subprocess.run('explorer {}'.format(os.path.dirname(local_destination.replace('/','\\'))))
        else:
            sg.Popup('Download failed')
    except:
        sg.Popup('something happened trying to download the file')


if __name__ == '__main__':
    main()
