# YAYDL (No Longer Developed)   
*2023-11-4 - Learned more since I started this and the code is bad, will create new project in future*   
---   
*Yet another youtube downloader*   

YAYDL is a simple Python wrapper for yt-dlp to make is easy to just download a quick video.   

This is meant for personal use so if you want to use it, feel free to do so but there is no warranty with this software and no promise that it won't ruin something on your computer.

**This is currently being redesigned from a bat script that was previously in use. It is not expected to work properly at this stage of development**
